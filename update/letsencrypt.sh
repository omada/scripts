#!/bin/sh
set -e

#
# this is a renew_hook script. it reloads nginx, then
# copies certfiles to the containers for prosody and mumble 
# it then lets import prosody the updated cert and restarts mumble
#
# == Warnings ==
# * be sure not to have a global post-hook for certbot as it will replace
#   the renew_hook configline
# * don't use the renewal-hooks/deploy or similar dir as it will be executed globally. 
#

# container name
container_mumble=
# well... (this script is intended for a single cert)
certname=$RENEWED_LINEAGE

echo ">>> reloading nginx"
systemctl reload nginx

echo ">>> pushing $container_mumble"
lxc file push $RENEWED_LINEAGE/privkey.pem $container_mumble/etc/letsencrypt/live/$certname/privkey.pem
echo ">>> pushed key, moving on"
lxc file push $RENEWED_LINEAGE/fullchain.pem $container_mumble/etc/letsencrypt/live/$certname/fullchain.pem
echo ">>> pushed cert"

echo ">>> restart mumble"
lxc exec $container_mumble -- systemctl restart mumble-server

echo ">>> all done, removing tmpdir"
# should not be needed, lxc command would work here too (see line 24)