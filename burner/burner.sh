#!/bin/bash


printf '
┌─────────────────────────────────────────────────────────────────────────────┐
│                                                                             │
│ [0;1;31;91m#[0m                                                    [0;1;36;96m#[0m           [0;1;36;96mm[0m          │
│ [0;1;33;93m#[0;1;32;92mmm[0;1;36;96mm[0m   [0;1;35;95mm[0m   [0;1;33;93mm[0m   [0;1;36;96mm[0m [0;1;34;94mmm[0m  [0;1;31;91mm[0m [0;1;33;93mmm[0m    [0;1;34;94mmm[0;1;35;95mm[0m    [0;1;33;93mm[0m [0;1;32;92mm[0;1;36;96mm[0m          [0;1;32;92mm[0;1;36;96mmm[0;1;34;94m#[0m   [0;1;31;91mmm[0;1;33;93mm[0m   [0;1;36;96mmm[0;1;34;94m#m[0;1;35;95mm[0m        │
│ [0;1;32;92m#[0;1;36;96m"[0m [0;1;34;94m"#[0m  [0;1;31;91m#[0m   [0;1;32;92m#[0m   [0;1;34;94m#"[0m  [0;1;31;91m"[0m [0;1;33;93m#"[0m  [0;1;36;96m#[0m  [0;1;34;94m#[0;1;35;95m"[0m  [0;1;31;91m#[0m   [0;1;32;92m#[0;1;36;96m"[0m  [0;1;34;94m"[0m        [0;1;36;96m#"[0m [0;1;34;94m"[0;1;35;95m#[0m  [0;1;31;91m#[0;1;33;93m"[0m [0;1;32;92m"#[0m    [0;1;35;95m#[0m          │
│ [0;1;36;96m#[0m   [0;1;35;95m#[0m  [0;1;33;93m#[0m   [0;1;36;96m#[0m   [0;1;35;95m#[0m     [0;1;32;92m#[0m   [0;1;34;94m#[0m  [0;1;35;95m#[0;1;31;91m""[0;1;33;93m""[0m   [0;1;36;96m#[0m            [0;1;34;94m#[0m   [0;1;31;91m#[0m  [0;1;33;93m#[0m   [0;1;36;96m#[0m    [0;1;31;91m#[0m          │
│ [0;1;34;94m#[0;1;35;95m#m[0;1;31;91m#"[0m  [0;1;32;92m"m[0;1;36;96mm"[0;1;34;94m#[0m   [0;1;31;91m#[0m     [0;1;36;96m#[0m   [0;1;35;95m#[0m  [0;1;31;91m"[0;1;33;93m#m[0;1;32;92mm"[0m   [0;1;34;94m#[0m            [0;1;35;95m"#[0;1;31;91mm#[0;1;33;93m#[0m  [0;1;32;92m"[0;1;36;96m#m[0;1;34;94m#"[0m    [0;1;33;93m"m[0;1;32;92mm[0m        │
│                                                                             │
│                                                                             │
│                                                                             │
│        [0;1;31;91m#[0m              [0;1;33;93mm[0;1;32;92mm[0m            [0;1;32;92mm[0;1;36;96mm[0m                   [0;1;33;93mmm[0;1;32;92mmm[0m               │
│  [0;1;34;94mmm[0;1;35;95mm[0m   [0;1;33;93m#[0m [0;1;32;92mmm[0m         [0;1;33;93mm[0;1;32;92m"[0m  [0;1;36;96m"[0;1;34;94mm[0m        [0;1;32;92mm[0;1;36;96m"[0m  [0;1;34;94m"[0;1;35;95mm[0m                [0;1;33;93m"[0m   [0;1;36;96m"[0;1;34;94m#[0m              │
│ [0;1;34;94m#[0m   [0;1;31;91m"[0m  [0;1;32;92m#"[0m  [0;1;34;94m#[0m                                       [0;1;31;91m#[0m      [0;1;36;96mm[0;1;34;94mmm[0;1;35;95m"[0m              │
│  [0;1;31;91m""[0;1;33;93m"m[0m  [0;1;36;96m#[0m   [0;1;35;95m#[0m                 [0;1;32;92m""[0;1;36;96m"[0m                            [0;1;35;95m"[0;1;31;91m#[0m              │
│ [0;1;31;91m"[0;1;33;93mmm[0;1;32;92mm"[0m  [0;1;34;94m#[0m   [0;1;31;91m#[0m                                       [0;1;32;92m#[0m    [0;1;34;94m"[0;1;35;95mmm[0;1;31;91mm#[0;1;33;93m"[0m              │
│                                                                             │
│                                                                             │
└─────────────────────────────────────────────────────────────────────────────┘
'





# FUNCTIONS HERE =======================================================================================================================
function checkadb {
ADBDEVICESOUTPUTCOUNT=$(adb devices | sed 's/devices//' | grep device | wc -l)
if [[ "ADBDEVICESOUTPUTCOUNT" -ne "1" ]]
 then
  echo 'ADB device not detected. Please make sure usb debugging is enabled and ensure your computer is authorized by the phone.'
  exit 1
 fi
}

function delay {
echo "Running in $COUNT seconds..."
cat /tmp/pkglist.txt
while [[ COUNT -gt 0 ]]
 do
printf $COUNT. ; sleep .25
printf '.' ; sleep .250
printf '.' ; sleep .250
echo '.' ; sleep .250
COUNT=$((COUNT - 1))
 done
}

function massrm {
adb shell 'cmd package list packages' | sed -e 's/package://' | sed -e 's/webview//' | sed -e 's/packageinstaller//' | grep $1 | tr ' ' '\n' > /tmp/pkglist.txt
PKGCOUNT=$(cat /tmp/pkglist.txt | wc -l)
PKGSRMD=1

while [[ $(($PKGSRMD - 1)) -lt "PKGCOUNT" ]]
 do
  echo '('$PKGSRMD'/'$PKGCOUNT')'
  NEXT=$(cat /tmp/pkglist.txt | sed "$PKGSRMD"'q;d')
  printf "$NEXT | "
  adb shell "pm uninstall --user 0 $NEXT"
  adb shell "pm disable --user 0 $NEXT"
  PKGSRMD=$((PKGSRMD + 1))
 done
}

function installfrom {
echo "Downloading from $1"
wget $1 -O /tmp/tmp.apk
echo 'Installing'
adb install /tmp/tmp.apk
}

function installfossifycollection {
installfromgh 'https://api.github.com/repos/FossifyOrg/Contacts/releases/latest'
installfromgh 'https://api.github.com/repos/FossifyOrg/Voice-Recorder/releases/latest'
installfromgh 'https://api.github.com/repos/FossifyOrg/Messages/releases/latest'
installfromgh 'https://api.github.com/repos/FossifyOrg/Gallery/releases/latest'
installfromgh 'https://api.github.com/repos/FossifyOrg/Calendar/releases/latest'
installfromgh 'https://api.github.com/repos/FossifyOrg/File-Manager/releases/latest'
installfromgh 'https://api.github.com/repos/FossifyOrg/Clock/releases/latest'
installfromgh 'https://api.github.com/repos/FossifyOrg/Notes/releases/latest'
installfromgh 'https://api.github.com/repos/FossifyOrg/Phone/releases/latest'
}
function installfromgh {
echo $1
echo $(curl -s $1 | grep "apk" | grep https:// | cut -d : -f 2,3 | tr -d \" | head -n 1)
installfrom $(curl -s $1 | grep "apk" | grep https:// | cut -d : -f 2,3 | tr -d \" | head -n 1)
}
function installtermux {
installfrom $(curl -s $1 | grep "apk" | grep https:// | cut -d : -f 2,3 | tr -d \" | grep universal | head -n 1)
}
function fucktelephony {
COUNT=10
delay
massrm telephony
massrm stk
massrm carrier
massrm verizon
massrm boostmobile
massrm att
massrm atnt
massrm tmobile
}

# UI HERE ===================================================================================================================================================================================================================
checkadb
printf '\x1b[1;31mEnsure all google accounts are removed from the device and then press enter. This script cannot remove them for you.\x1b[1;0m'
read


function massrmmenu {

printf '
[1] Remove all telephony/carrier related applications
[2] remove all google related packages - this WILL break google play services and notifications!
'
printf '\x1b[1;36m>\x1b[1;0m'
read USERCHOICE

case $USERCHOICE in
  '1') fucktelephony ;;
  '2') COUNT=10; delay; massrm 'google';;
esac
}


function appinstallmenu {
printf 'Heres the selection. You can only install one app at a time, I hope you have good internet
For collections like fossify, 5 will install the whole collection and 5.x will install a specific app.
[1] Molly
[2] F-Droid - broken
[3] Aliucord Installer
[4] [Reserved]
[5] Fossify collection [contacts, voice recorder, SMS, gallery, calender, file manager, clock, notes, phone]
 [5.1] Fossify Contacts
 [5.2] Fossify Voice Recorder
 [5.3] Fossify SMS
 [5.4] Fossify Gallery
 [5.5] Fossify Calender
 [5.6] Fossify File Manager
 [5.7] Fossify Clock
 [5.8] Fossify Notes
 [5.9] Fossify Phone
[6] Metro Music
[7] Termux
[8] Newpipe
[9] Moshidon
'
printf '\x1b[1;36m>\x1b[1;0m'
read USERCHOICE

case "$USERCHOICE" in
 '1') installfromgh 'https://api.github.com/repos/mollyim/mollyim-android/releases/latest';;
 '2') 'echo "Couldnt find a way to scrape the latest APK. Find it on your own."';;
 '3') installfromgh 'https://api.github.com/repos/Aliucord/Aliucord/releases/latest' ;;
 '4') echo 'being left empty' ;;
 '5') installfossifycollection;;
 '5.1') installfromgh 'https://api.github.com/repos/FossifyOrg/Contacts/releases/latest';;
 '5.2') installfromgh 'https://api.github.com/repos/FossifyOrg/Voice-Recorder/releases/latest';;
 '5.3') installfromgh 'https://api.github.com/repos/FossifyOrg/Messages/releases/latest';;
 '5.4') installfromgh 'https://api.github.com/repos/FossifyOrg/Gallery/releases/latest';;
 '5.5') installfromgh 'https://api.github.com/repos/FossifyOrg/Calendar/releases/latest';;
 '5.6') installfromgh 'https://api.github.com/repos/FossifyOrg/File-Manager/releases/latest';;
 '5.7') installfromgh 'https://api.github.com/repos/FossifyOrg/Clock/releases/latest';;
 '5.8') installfromgh 'https://api.github.com/repos/FossifyOrg/Notes/releases/latest';;
 '5.9') installfromgh 'https://api.github.com/repos/FossifyOrg/Phone/releases/latest';;
 '6') installfrom 'https://f-droid.org/repo/io.github.muntashirakon.Music_10603.apk';; # I know this is dumb. I know. But, its been a year since the last release.
 '7') installtermux 'https://api.github.com/repos/termux/termux-app/releases/latest';;
 '8') installfromgh 'https://api.github.com/repos/TeamNewPipe/NewPipe/releases/latest';;
 '9') installfromgh 'https://api.github.com/repos/LucasGGamerM/moshidon/releases/latest';;
esac
}


function mainmenu {
printf 'Welcome to burner.sh, select what you want.
[1] App Purging
[2] App Installs
'

printf '\x1b[1;36m>\x1b[1;0m'
read USERCHOICE

case "$USERCHOICE" in
  1) 'massrmmenu';;
  2) 'appinstallmenu';;
esac
exit 0
}


mainmenu
